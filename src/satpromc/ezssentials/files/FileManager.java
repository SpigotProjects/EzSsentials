package satpromc.ezssentials.files;

import java.util.ArrayList;

public class FileManager {

    private static FileManager instance;

    public static FileManager getInstance() {
        if (instance == null){
            instance = new FileManager();
        }
        return instance;
    }

    public ArrayList<EzFile> files = new ArrayList<>();

    public FileManager(){
        files.add(player_data);
    }

    public EzFile player_data = new EzFile("player_data.yml");



}
