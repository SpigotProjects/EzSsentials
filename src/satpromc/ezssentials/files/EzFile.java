package satpromc.ezssentials.files;

import org.bukkit.configuration.file.YamlConfiguration;
import satpromc.ezssentials.Core;

import java.io.File;
import java.io.IOException;

public class EzFile {

    private File file;
    private YamlConfiguration config;

    public EzFile(String fileName) {
        file = new File(Core.getInstance().getDataFolder(), fileName);
        config = YamlConfiguration.loadConfiguration(file);
    }

    public File getFile() {
        return file;
    }

    public YamlConfiguration getYml() {
        return config;
    }

    public void setupFile() {
        if (file.exists()) return;
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            config.save(file);
            //DEBUG
            // System.out.println(file.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
