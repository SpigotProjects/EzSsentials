package satpromc.ezssentials.data;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import satpromc.ezssentials.files.FileManager;

public class DataManager {

    private static DataManager instance;
    private FileManager fm = FileManager.getInstance();
    private YamlConfiguration playerData = fm.player_data.getYml();

    public static DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }

    public void createPlayer(Player p) {

        String uuid = p.getUniqueId().toString();

        if (playerData.get(uuid + ".fly_enabled") == null) {
            playerData.set(uuid + ".fly_enabled", false);
        }
        if (playerData.get(uuid + ".gamemode") == null) {
            playerData.set(uuid + ".gamemode", 0);
        }
        if (playerData.get(uuid + ".health") == null) {
            playerData.set(uuid + ".health", 20);
        }
        if (playerData.get(uuid + ".max_health") == null){
            playerData.set(uuid + ".max_health", 20);
        }
        if (playerData.get(uuid + ".food_lvl") == null){
            playerData.set(uuid + ".food_lvl", 20);
        }
        fm.player_data.save();
    }

    public boolean playerExists(Player p) {
        if (playerData.get(p.getUniqueId().toString()) == null) {
            return false;
        }
        return true;
    }

}
