package satpromc.ezssentials;

import org.bukkit.plugin.java.JavaPlugin;
import satpromc.ezssentials.commands.cheats.FeedCommand;
import satpromc.ezssentials.commands.cheats.HealCommand;
import satpromc.ezssentials.commands.player.FlyCommand;
import satpromc.ezssentials.events.PlayerJoinLeave;
import satpromc.ezssentials.files.EzFile;
import satpromc.ezssentials.files.FileManager;
import satpromc.ezssentials.utils.ChatUtil;

public class Core extends JavaPlugin {

    private static Core instance;

    public static Core getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;

        getConfig().options().copyDefaults(true);
        saveConfig();

        registerCommands();
        registerListeners();

        new FileManager();
        for (EzFile file : FileManager.getInstance().files){
            file.setupFile();
        }
    }

    // VARIABLES
    public String mainColor = ChatUtil.format(getConfig().getString("primary-color"));
    public String secColor = ChatUtil.format(getConfig().getString("secondary-color"));


    @Override
    public void onDisable() {
        instance = null;
    }

    private void registerCommands() {
        getCommand("fly").setExecutor(new FlyCommand());
        getCommand("heal").setExecutor(new HealCommand());
        getCommand("feed").setExecutor(new FeedCommand());
    }

    private void registerListeners() {
        this.getServer().getPluginManager().registerEvents(new PlayerJoinLeave(), this);
    }
}
