package satpromc.ezssentials.events;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import satpromc.ezssentials.Core;
import satpromc.ezssentials.data.DataManager;
import satpromc.ezssentials.files.FileManager;
import satpromc.ezssentials.utils.ChatUtil;
import satpromc.ezssentials.utils.GameModeUtils;

@SuppressWarnings("unused")
public class PlayerJoinLeave implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        FileConfiguration config = Core.getInstance().getConfig();

        String join_message = ChatUtil.format(config.getString("player-join-message")).replace
                ("%%player%%", p.getName().replace("%%disp_player%%", p.getDisplayName()));
        e.setJoinMessage(join_message);

        // MOTD
        String motd = ChatUtil.format(config.getString("player-join-motd")).replace
                ("%%player%%", p.getName().replace("%%disp_player%%", p.getDisplayName())).replace("%%next_line%%", "\n");
        p.sendMessage(motd);

        DataManager.getInstance().createPlayer(p);
        if (!DataManager.getInstance().playerExists(p)){
            firstJoin(p);
        }

        YamlConfiguration playerData = FileManager.getInstance().player_data.getYml();
        // GIVE PROPERTIES
        p.setGameMode(GameModeUtils.getGMFromInt(playerData.getInt(p.getUniqueId().toString() + ".gamemode")));
        p.setHealth(playerData.getDouble(p.getUniqueId().toString() + ".health"));
        p.setAllowFlight(playerData.getBoolean(p.getUniqueId().toString() + ".fly_enabled"));


    }

    private void firstJoin(Player p){
        YamlConfiguration playerData = FileManager.getInstance().player_data.getYml();
        FileConfiguration config = Core.getInstance().getConfig();
        // SET GAMEMODE
        p.setGameMode(GameModeUtils.getGMFromInt(config.getInt("default-gamemode")));
        // WELCOME MESSAGE
        Bukkit.broadcastMessage(ChatUtil.format(config.getString("player-join-first-time-message")).replace
                ("%%player%%", p.getName().replace("%%disp_player%%", p.getDisplayName())).replace("%%next_line%%", "\n"));

    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {

        Player p = e.getPlayer();
        FileConfiguration config = Core.getInstance().getConfig();

        String quit_message = ChatUtil.format(config.getString("player-leave-message")).replace
                ("%%player%%", p.getName().replace("%%disp_player%%", p.getDisplayName()));
        e.setQuitMessage(quit_message);


    }

}
