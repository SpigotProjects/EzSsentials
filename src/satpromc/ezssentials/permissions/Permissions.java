package satpromc.ezssentials.permissions;

import satpromc.ezssentials.ExecutionType;
import static satpromc.ezssentials.ExecutionType.*;

@SuppressWarnings("unused")
public enum Permissions {

    FLY(COMMAND, "ezssentials.commands.fly", "ezssentials.commands.fly.other"),
    HEAL(COMMAND, "ezssentials.commands.heal", "ezssentials.commands.heal.other"),
    FFED(COMMAND, "ezssentials.commands.feed", "ezssentials.commands.feed.other");

    String permission;
    String permissionOnOther;
    ExecutionType type;

    Permissions(ExecutionType type, String permission) {
        this.type = type;
        this.permission = permission;
    }

    Permissions(ExecutionType type, String permission, String permissionOnOther) {
        this.type = type;
        this.permission = permission;
        this.permissionOnOther = permissionOnOther;
    }



    public ExecutionType getExecutionType() {
        return type;
    }

    public String getPermissionNode(){
        return permission;
    }

    public String getPermissionOnOther() {
        return permissionOnOther;
    }

}

