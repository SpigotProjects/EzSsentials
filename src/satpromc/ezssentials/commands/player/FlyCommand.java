package satpromc.ezssentials.commands.player;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import satpromc.ezssentials.Core;
import satpromc.ezssentials.files.EzFile;
import satpromc.ezssentials.files.FileManager;
import satpromc.ezssentials.permissions.Permissions;
import satpromc.ezssentials.utils.ChatUtil;
import satpromc.ezssentials.utils.DefaultMessages;

public class FlyCommand implements CommandExecutor {

    // SOME NullPointerExceptions may occur!
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase("fly")) {
            if (!(sender instanceof Player)) {
                // THEY ARE NOT A PLAYER
                if (args.length != 1) {
                    sender.sendMessage(DefaultMessages.IMPROPER_PARAMETERS.getMessage());
                    return true;
                }

                Player target = Bukkit.getPlayer(args[0]);
                YamlConfiguration playerData = FileManager.getInstance().player_data.getYml();
                EzFile pData = FileManager.getInstance().player_data;
                if (playerData.get(target.getUniqueId().toString()) == null) {
                    sender.sendMessage(ChatColor.RED + "I do not know who " + args[0] + " is.");
                    return true;
                }
                if (target.isOnline()) {
                    if (target.getAllowFlight()) {
                        target.setAllowFlight(false);
                        target.sendMessage(ChatUtil.format(Core.getInstance().mainColor + "Your " + Core.getInstance().secColor + "flight" + Core.getInstance().mainColor + " has been set to " +
                                Core.getInstance().secColor + "false" + Core.getInstance().mainColor + "."));
                        sender.sendMessage(ChatUtil.format(Core.getInstance().mainColor + target.getName() + "'s " + Core.getInstance().secColor + "flight" + Core.getInstance().mainColor + " has been set to " +
                                Core.getInstance().secColor + "false" + Core.getInstance().mainColor + "."));
                        playerData.set(target.getUniqueId().toString() + ".fly_enabled", false);
                        pData.save();
                    } else {
                        target.setAllowFlight(true);
                        target.sendMessage(ChatUtil.format(Core.getInstance().mainColor + "Your " + Core.getInstance().secColor + "flight" + Core.getInstance().mainColor + " has been set to " +
                                Core.getInstance().secColor + "true" + Core.getInstance().mainColor + "."));
                        sender.sendMessage(ChatUtil.format(Core.getInstance().mainColor + target.getName() + "'s " + Core.getInstance().secColor + "flight" + Core.getInstance().mainColor + " has been set to " +
                                Core.getInstance().secColor + "true" + Core.getInstance().mainColor + "."));
                        playerData.set(target.getUniqueId().toString() + ".fly_enabled", true);
                        pData.save();
                    }
                } else {
                    if (playerData.getBoolean(target.getUniqueId().toString() + ".fly_enabled")) {
                        playerData.set(target.getUniqueId().toString() + ".fly_enabled", false);
                        pData.save();
                        sender.sendMessage(ChatUtil.format(Core.getInstance().mainColor + target.getName() + "'s " + Core.getInstance().secColor + "flight " + Core.getInstance().mainColor + "has been set to " +
                                Core.getInstance().secColor + "false" + Core.getInstance().mainColor + "."));
                    } else {
                        playerData.set(target.getUniqueId().toString() + ".fly_enabled", true);
                        pData.save();
                        sender.sendMessage(ChatUtil.format(Core.getInstance().mainColor + target.getName() + "'s " + Core.getInstance().secColor + "flight " + Core.getInstance().mainColor + "has been set to " +
                                Core.getInstance().secColor + "true" + Core.getInstance().mainColor + "."));
                    }
                }

            } else {
                // THEY ARE A PLAYER
                Player p = (Player) sender;
                YamlConfiguration playerData = FileManager.getInstance().player_data.getYml();
                EzFile pData = FileManager.getInstance().player_data;
                if (args.length == 0){
                    if (!p.hasPermission(Permissions.FLY.getPermissionNode())){
                        sender.sendMessage(DefaultMessages.NO_PERMISSION.getMessage());
                        return true;
                    }
                    if (p.getAllowFlight()){
                        sender.sendMessage(ChatUtil.format(Core.getInstance().mainColor + "Your " + Core.getInstance().secColor + "flight" + Core.getInstance().mainColor + " has been set to " +
                                Core.getInstance().secColor + "false" + Core.getInstance().mainColor + "."));
                        playerData.set(p.getUniqueId().toString() + ".fly_enabled", false);
                        pData.save();
                        p.setAllowFlight(false);
                    } else {
                        sender.sendMessage(ChatUtil.format(Core.getInstance().mainColor + "Your " + Core.getInstance().secColor + "flight" + Core.getInstance().mainColor + " has been set to " +
                                Core.getInstance().secColor + "true" + Core.getInstance().mainColor + "."));
                        playerData.set(p.getUniqueId().toString() + ".fly_enabled", true);
                        pData.save();
                        p.setAllowFlight(true);
                    }
                } else {
                    if (args.length != 1){
                        p.sendMessage(DefaultMessages.IMPROPER_PARAMETERS.getMessage());
                        return true;
                    }
                    if (!p.hasPermission(Permissions.FLY.getPermissionOnOther())){
                        sender.sendMessage(DefaultMessages.NO_PERMISSION.getMessage());
                        return true;
                    }

                    Player target = Bukkit.getPlayer(args[0]);
                    if (target.isOnline()) {
                        if (target.getAllowFlight()) {
                            target.setAllowFlight(false);
                            target.sendMessage(ChatUtil.format(Core.getInstance().mainColor + "Your " + Core.getInstance().secColor + "flight " + Core.getInstance().mainColor + "has been set to " +
                                    Core.getInstance().secColor + "false" + Core.getInstance().mainColor + "."));
                            sender.sendMessage(ChatUtil.format(Core.getInstance().mainColor + target.getName() + "'s " + Core.getInstance().secColor + "flight " + Core.getInstance().mainColor + "has been set to " +
                                    Core.getInstance().secColor + "false" + Core.getInstance().mainColor + "."));
                            playerData.set(target.getUniqueId().toString() + ".fly_enabled", false);
                            pData.save();
                        } else {
                            target.setAllowFlight(true);
                            target.sendMessage(ChatUtil.format(Core.getInstance().mainColor + "Your " + Core.getInstance().secColor + "flight " + Core.getInstance().mainColor + "has been set to " +
                                    Core.getInstance().secColor + "true" + Core.getInstance().mainColor + "."));
                            sender.sendMessage(ChatUtil.format(Core.getInstance().mainColor + target.getName() + "'s" + Core.getInstance().secColor + "flight" + Core.getInstance().mainColor + "has been set to " +
                                    Core.getInstance().secColor + "true" + Core.getInstance().mainColor + "."));
                            playerData.set(target.getUniqueId().toString() + ".fly_enabled", true);
                            pData.save();
                        }
                    } else {
                        if (playerData.getBoolean(target.getUniqueId().toString() + ".fly_enabled")) {
                            playerData.set(target.getUniqueId().toString() + ".fly_enabled", false);
                            pData.save();
                            sender.sendMessage(ChatUtil.format(Core.getInstance().mainColor + target.getName() + "'s " + Core.getInstance().secColor + "flight " + Core.getInstance().mainColor + "has been set to " +
                                    Core.getInstance().secColor + "false" + Core.getInstance().mainColor + "."));
                        } else {
                            playerData.set(target.getUniqueId().toString() + ".fly_enabled", true);
                            pData.save();
                            sender.sendMessage(ChatUtil.format(Core.getInstance().mainColor + target.getName() + "'s " + Core.getInstance().secColor + "flight " + Core.getInstance().mainColor + "has been set to " +
                                    Core.getInstance().secColor + "true" + Core.getInstance().mainColor + "."));
                        }
                    }
                }
            }
        }
        return true;
    }


}
