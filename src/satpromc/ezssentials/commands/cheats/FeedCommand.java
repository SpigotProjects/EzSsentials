package satpromc.ezssentials.commands.cheats;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import satpromc.ezssentials.Core;
import satpromc.ezssentials.files.EzFile;
import satpromc.ezssentials.files.FileManager;
import satpromc.ezssentials.permissions.Permissions;
import satpromc.ezssentials.utils.DefaultMessages;

public class FeedCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String mainC = Core.getInstance().mainColor;
        String secC = Core.getInstance().secColor;
        YamlConfiguration pD = FileManager.getInstance().player_data.getYml();
        EzFile plD = FileManager.getInstance().player_data;
        if (label.equalsIgnoreCase("feed")) {
            if (!(sender instanceof Player)) {
                if (args.length != 1) {
                    sender.sendMessage(DefaultMessages.IMPROPER_PARAMETERS.getMessage());
                    return true;
                }
                Player target = Bukkit.getPlayer(args[0]);
                String sUUID = target.getUniqueId().toString();
                if (target.isOnline()) {
                    target.setFoodLevel(20);
                    pD.set(sUUID + ".food_lvl", 20);
                    plD.save();
                    target.sendMessage(mainC + "You have been " + secC + " fed" + mainC + ".");
                    sender.sendMessage(mainC + target.getName() + " has been " + secC + "fed" + mainC + ".");
                    return true;
                } else {
                    pD.set(sUUID + ".food_lvl", 20);
                    plD.save();
                    sender.sendMessage(mainC + target.getName() + " has been " + secC + "fed" + mainC + ".");
                    return true;
                }
            } else {
                // THEY ARE A PLAYER
                Player p = (Player) sender;
                if (args.length == 0){
                    if (!p.hasPermission(Permissions.HEAL.getPermissionNode())){
                        p.sendMessage(DefaultMessages.NO_PERMISSION.getMessage());
                        return true;
                    }
                    p.setFoodLevel(20);
                    p.sendMessage(mainC + "You " + secC + "fed " + mainC + "yourself.");
                    pD.set(p.getUniqueId().toString() + ".food_lvl", 20);
                    plD.save();
                    return true;
                }
                if (args.length != 1){
                    p.sendMessage(DefaultMessages.IMPROPER_PARAMETERS.getMessage());
                    return true;
                }
                Player target = Bukkit.getPlayer(args[0]);
                String sUUID = target.getUniqueId().toString();
                if (target.isOnline()) {
                    target.setHealth(target.getMaxHealth());
                    pD.set(sUUID + ".food_lvl", 20);
                    plD.save();
                    target.sendMessage(mainC + "You have been " + secC + " fed" + mainC + ".");
                    sender.sendMessage(mainC + target.getName() + " has been " + secC + "fed" + mainC + ".");
                    return true;
                } else {
                    pD.set(sUUID + ".food_lvl", 20);
                    plD.save();
                    sender.sendMessage(mainC + target.getName() + " has been " + secC + "fed" + mainC + ".");
                    return true;
                }

            }
        }
        return true;
    }


}

