package satpromc.ezssentials.utils;

import org.bukkit.GameMode;
import static org.bukkit.GameMode.*;

public class GameModeUtils {

    public static GameMode getGMFromInt(int gm){
        switch (gm){
            case 0:
                return SURVIVAL;
            case 1:
                return CREATIVE;
            case 2:
                return ADVENTURE;
            case 3:
                return SPECTATOR;
            default:
                System.out.println("[ERROR] getGMFromInt() has a number less than 0 or greater than 3. ");
                return SURVIVAL;
        }
    }



}
