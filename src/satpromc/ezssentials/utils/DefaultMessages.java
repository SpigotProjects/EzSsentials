package satpromc.ezssentials.utils;

public enum DefaultMessages {

    NO_PERMISSION(ChatUtil.format("&cYou do not have permission to do this.")),
    PLAYER_ONLY(ChatUtil.format("&cYou must be a player to do this.")),
    IMPROPER_PARAMETERS(ChatUtil.format("Improper parameters given."));

    String message;
    DefaultMessages(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

}
